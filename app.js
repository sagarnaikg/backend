const express=require('express');
const app=express();

app.use(express.json());

const user={
    email:"abc@abc.com",
    password:"123456",
}

// initial connect
app.get('/',(req,res)=>{
    res.json({ status: "Connected" });
});

// login 
app.put('/auth',(req,res)=>{

    const req_user={
        email:req.body.email,
        password:req.body.password,
    };

    if(req_user.email==user.email && req_user.password==user.password){
        res.json({ status: "Valid" });
    } else  res.json({ status: "Invalid" });;
    
});

// change password
app.put('/auth/changePassword',(req,res)=>{

    const req_user={
        oldPassword:req.body.old,
        newPassword:req.body.new,
    };
    
    res.json({ status: "success" });

});

const port=process.env.PORT || 3000

app.listen(port,()=>console.log(`started at ${port}`));